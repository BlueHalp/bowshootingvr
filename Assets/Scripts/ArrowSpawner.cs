using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
    using Random=UnityEngine.Random;


public class ArrowSpawner : MonoBehaviour
{

    public List<GameObject> arrowTypes = new List<GameObject>();
    public GameObject arrow;
    public GameObject notch;

    private XRGrabInteractable bow;
    private bool arrowNotched = false;
    private GameObject actualArrow = null;

    void Start()
    {
        bow = GetComponent<XRGrabInteractable>();
        PullBowInteraction.pullActionRelease += NotchEmpty;
    }

    private void OnDestroy()
    {
        PullBowInteraction.pullActionRelease -= NotchEmpty;
    }

    void Update()
    {
        if (bow.isSelected && arrowNotched == false)
        {
            arrowNotched = true;
            StartCoroutine("DelaySpawn");
        }
        if (!bow.isSelected && actualArrow != null)
        {
            Destroy(actualArrow);
            NotchEmpty(1f);
        }
        if (actualArrow != null && arrowNotched)
        {
            actualArrow.transform.position = new Vector3(notch.transform.position.x, notch.transform.position.y, notch.transform.position.z);
            actualArrow.transform.rotation = bow.transform.rotation;
        }
    }

    private void NotchEmpty(float value)
    {
        arrowNotched = false;
        actualArrow = null;
    }

    IEnumerator DelaySpawn()
    {
        yield return new WaitForSeconds(1f);
        int randomNumber = Random.Range(0, arrowTypes.Count);
        arrow = arrowTypes[randomNumber];
        actualArrow = Instantiate(arrow, notch.transform);
    }
}
