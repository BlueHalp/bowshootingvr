using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class MovingTargetScript : MonoBehaviour
{

    [SerializeField] GameObject movableObject;

    Boolean isLeft;

    Boolean isMoving;

    void Start()
    {
        isLeft = false;
        isMoving = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isMoving)
        {
            return;
        }

        if (movableObject.transform.position.z > 3 && !isLeft)
        {
            movableObject.transform.position += Vector3.back / 100;
        }
        else if (movableObject.transform.position.z >= 2.9 && !isLeft)
        {
            isLeft = true;
        }
        if (movableObject.transform.position.z < 9 && isLeft)
        {
            movableObject.transform.position -= Vector3.back / 100;
        }
        else if (movableObject.transform.position.z >= 9 && isLeft)
        {
            isLeft = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        isMoving = false;
    }

}
