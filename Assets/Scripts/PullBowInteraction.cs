using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PullBowInteraction : XRBaseInteractable
{

    public static event Action<float> pullActionRelease;

    public GameObject notch;

    /*
     * Coords inicio de cuerda, final de cuerda (dependiendo de la tension que se ejerce)
     */
    public Transform start, end;

    public float pullAmount { get; private set; } = 0.0f;
    /*
     * La cuerda del arco
     */
    public LineRenderer stringlineRenderer;

    /*
     * La mano que est� cogiendo el arco
     */
    private IXRSelectInteractor pullInteractor = null;

    protected override void Awake()
    {
        base.Awake();
    }

    public void SetPullInteractor(SelectEnterEventArgs arguments)
    {
        pullInteractor = arguments.interactorObject;
    }

    /*
     * Cuando soltamos la cuerda y vuelve a su posicion original
     */
    public void Release()
    {
        pullActionRelease?.Invoke(pullAmount);
        pullInteractor = null;
        pullAmount = 0.0f;
        notch.transform.localPosition = new Vector3(notch.transform.localPosition.x, notch.transform.localPosition.y, 0f);
        updateBowString();
    }

    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        base.ProcessInteractable(updatePhase);
        /*
         * Mirar si es dinamico (ya que es cuando se puede mover, animar, modificar, etc. el objeto
        */
        if (updatePhase == XRInteractionUpdateOrder.UpdatePhase.Dynamic)
        {
            if (isSelected)
            {
                Vector3 pullPosition = pullInteractor.transform.position;
                pullAmount = CalculatePull(pullPosition);

                updateBowString();
            }
        }
    }

    /*
     * Sacar la distancia de la cuerda desde su punto de origen, y dependiendo de la distancia devolver la "tension" que se ejerce en la cuerda
     */ 
   private float CalculatePull(Vector3 pullPosition)
    {
        Vector3 pullDirection = pullPosition - start.position;
        Vector3 targetDirection = end.position - start.position;
        float maximumLenght = targetDirection.magnitude;

        targetDirection.Normalize();
        float pullValue = Vector3.Dot(pullDirection, targetDirection) / maximumLenght;

        //Vector3 force = transform.forward * pullValue * 30;
        //ShowTrayectoryLine(transform.position, force);

        return Math.Clamp(pullValue, 0, 1);
    }

    /*
     * Actualiza la cuerda para mostrar en el objeto la distancia y tension que hay
     */
    private void updateBowString()
    {
        Vector3 linePosition = Vector3.forward * Mathf.Lerp(start.transform.localPosition.z, end.transform.localPosition.z, pullAmount);
        notch.transform.localPosition = new Vector3(notch.transform.localPosition.x, notch.transform.localPosition.y, linePosition.z + .2f);
        stringlineRenderer.SetPosition(1, linePosition);
    }
}
