using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointsCalculationHandler : MonoBehaviour
{
    public static event Action<float> points;
    [SerializeField] public TMP_Text scoreDisplay;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreDisplay.text = "Points: " + points;
        
    }
}
