using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ArrowScript : MonoBehaviour
{
    public float speed = 30f;
    public Transform tip;
    
    public Rigidbody rb;
    private bool inAir = false;
    private Vector3 lastPosition = Vector3.zero;
    private float points = 0f;

    [SerializeField] public LineRenderer lineRenderer;

    [SerializeField] private int linesSegments = 60;

    [SerializeField] private float flightTime = 5;

    private void Awake()
    {
        PullBowInteraction.pullActionRelease += Release;
        Stop();
    }

    private void OnDestroy()
    {
        PullBowInteraction.pullActionRelease -= Release;
    }

    private void Release(float value)
    {
        PullBowInteraction.pullActionRelease -= Release;
        gameObject.transform.parent = null;
        inAir = true;
        SetPhysics(true);

        Vector3 force = transform.forward * value * speed;
        ShowTrayectoryLine(tip.position, force);


        StartCoroutine(FadeOutTrayectoryLine());

        rb.AddForce(force, ForceMode.Impulse);

        StartCoroutine(RotateWithVelocity());
       
        lastPosition = tip.position;
    }

    /*
     * Hace rotar la flecha en caso de que caiga hacia abajo
     */
    private IEnumerator RotateWithVelocity()
    {
        yield return new WaitForFixedUpdate();
        while (inAir)
        {
            Quaternion newRotation = Quaternion.LookRotation(rb.velocity, transform.up);
            transform.rotation = newRotation;
            yield return null;
        }
    }

    private IEnumerator FadeOutTrayectoryLine()
    {
        yield return new WaitForSeconds(1);
        
       lineRenderer.positionCount = 0;
    }

    void FixedUpdate()
    {
        if (inAir)
        {
            CheckCollision();
            lastPosition = tip.position;
        }
    }
    private void CheckCollision()
    {
        if (Physics.Linecast(lastPosition, tip.position, out RaycastHit hitInfo))
        {
            if (hitInfo.transform.gameObject.layer != 8)
            {
                if (hitInfo.transform.TryGetComponent(out Rigidbody body))
                {
                    rb.interpolation = RigidbodyInterpolation.None;
                    transform.parent = hitInfo.transform;
                    body.AddForce(rb.velocity, ForceMode.Impulse);
                    

                    UpdateScore(body.GetComponent<Collider>());
                }
                Stop();
            }
        }
    }
    private void Stop()
    {
        inAir = false;
        SetPhysics(false);
    }

    private void SetPhysics(bool usePhysics)
    {
        rb.useGravity = usePhysics;
    }

    private void UpdateScore(Collider RBCollider)
    {
        Vector3 center = RBCollider.bounds.center;
        float precision = Vector3.Distance(center, tip.position);

        float size = RBCollider.bounds.size.sqrMagnitude;
        points += (precision + size);

    }

    public void ShowTrayectoryLine(Vector3 startPoint, Vector3 startVelocity)
    {
        float timeStep = flightTime / linesSegments;
        Vector3[] lineRendererPoints = CalculateTrajectoryLine(startPoint, startVelocity, timeStep);

        lineRenderer.positionCount = linesSegments;
        lineRenderer.SetPositions(lineRendererPoints);
    }

    private Vector3[] CalculateTrajectoryLine(Vector3 startPoint, Vector3 startVelocity, float timeStep)
    {
        Vector3[] lineRendererPoints = new Vector3[linesSegments];
        lineRendererPoints[0] = startPoint;

        for (int i = 0; i < linesSegments; i++)
        {
            float timeOffset = timeStep * i;

            Vector3 progressBeforeGravity = startVelocity * timeOffset;
            Vector3 gravityOffset = Vector3.up * -0.5f * Physics.gravity.y * timeOffset * timeOffset;
            Vector3 newPosition = startPoint + progressBeforeGravity - gravityOffset;

            lineRendererPoints[i] = newPosition;
        }

        return lineRendererPoints;
    }
}
